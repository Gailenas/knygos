<!DOCTYPE html>
<html>
<head>
    <title>Knygos</title>
    <meta charset="utf-8">
    <link   rel="stylesheet" 
            href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" 
            integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" 
            crossorigin="anonymous"
            >
    <link   rel="stylesheet" 
            type="text/css" 
            href="styles.css"
            >
</head>
<body>
    <div class="row">
        <div class="col-md-6 col-sm-12 attach">
            <form action="attach.php" method="post">
                <table>
                    <tr>
                        <th>Pavadinimas: </th>
                        <td><input class="pull-right" type="text" name="pavadinimas"></td>
                    </tr>
                    <tr>
                        <th>Metai: </th>
                        <td><input class="pull-right" type="date" name="metai"></td>
                    </tr>
                    <tr>
                        <th>Autorius: </th>
                        <td><input class="pull-right" type="text" name="autorius"></td>
                    </tr>
                    <tr>
                        <th>Žanras: </th>
                        <td><input class="pull-right" type="text" name="zanras"></td>
                    </tr>
                    <tr>
                        <th>Aprašymas: </th>
                        <td><input class="pull-right" type="text" name="aprasymas"></td>
                    </tr>
                    <tr>
                        <th></th>
                        <td><input class="pull-right" type="submit" name="submit" value="Pridėti"></td>
                    </tr>
                </table>
            </form>
        </div>
    <?php

        $servername     = "localhost";
        $username       = "agstas_admin";
        $password       = "admin1234567";
        $dbname         = "agstas_knygos";

        $conn = new mysqli( $servername, 
                            $username, 
                            $password, 
                            $dbname
                            );

        mysqli_set_charset($conn,"utf-8");

        if ($conn->connect_error) 
        {
            die("Sujungimas nepavyko: " . $conn->connect_error);
        }   
            echo '<div class="col-md-6 col-sm-12 search">';
                echo '<form action="index.php" method="GET">';
                    echo '<input class="pull-right width" type="text" name="keyword" placeholder="Paieška pagal pavadinimą ir autorių">';
                    echo '<input class="pull-right" type="submit" name="submit" value="submit">';
                echo '</form>';
            echo '</div>';
        echo '</div>';

        echo '<div class="list list-wrapper">';
            echo '<div class="nr">Nr.</div>';
            echo '<div class="name">Pavadinimas<a href="index.php?sort=pavadinimasAscending"><</a><a href="index.php?sort=pavadinimasDescending">></a></div>';
            echo '<div class="years">Metai<a href="index.php?sort=metaiAscending"><</a><a href="index.php?sort=metaiDescending">></a></div>';
            echo '<div class="autorius">Autorius<a href="index.php?sort=autoriusAscending"><</a><a href="index.php?sort=autoriusDescending">></a></div>';
            echo '<div class="zanras">Žanras</div>';
        echo "</div>";

        $sql = "select * FROM knygos ";
            if (isset($_GET['keyword']))
            {
                $keyword =  $_GET['keyword'];
                $sql .= "WHERE Pavadinimas LIKE '%$keyword%' OR Autorius LIKE '%$keyword%' ";
            }

            if(isset($_GET['sort'])){
                if($_GET['sort'] == 'pavadinimasAscending')
                    $sql .= 'ORDER BY Pavadinimas';

                if($_GET['sort'] == 'pavadinimasDescending')
                    $sql .= 'ORDER BY Pavadinimas desc';

                if($_GET['sort'] == 'metaiAscending')
                    $sql .= 'ORDER BY Metai';

                if($_GET['sort'] == 'metaiDescending')
                    $sql .= 'ORDER BY Metai desc';

                if($_GET['sort'] == 'autoriusDescending')
                    $sql .= 'ORDER BY Autorius';

                if($_GET['sort'] == 'autoriusAscending')
                    $sql .= 'ORDER BY Autorius desc';
            }
        $result = $conn->query($sql);
        $i = 0;
        if ($result->num_rows > 0) 
        {
            while($row = $result->fetch_assoc()) 
            {
            	$i++;
                echo ' <div class="list  onmouseover"  onclick="location.href=\'knygos.php?ID='.$row['ID'].'\'">';
                	echo '<div class="nr">'.$i .'</div>';
                	echo '<div class="name">'.$row['Pavadinimas'] .'</div>';
                	echo '<div class="years">'.$row['Metai'] .'</div>';
                	echo '<div class="nr">'.$row['Autorius'] .'</div>';
                	echo '<div class="nr"> '.$row['Zanras'] .'</div>';
                echo ' </div>';
           }
        } else 
        {
        echo "Nėra rezultatų";
        }
        $conn->close();
    ?>
</body>
</html>